<login>
    <div class="col-md-6">

        <h1>Login</h1>
            <!-- GET by TYPE -->
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="playerName">Username</label>
                    <div class="col-md-8">
                        <input id="loginUsername" type="text" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="playerName">Password</label>
                    <div class="col-md-8">
                        <input id="loginPassword" type="password" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="col-md-3">
                    <button id="loginButton" class="btn btn-primary btn-block" type="button" onclick={ loginButton }>LogIn</button>
                </div>
            </div>
    </div>
<script>
//User logs in
//$('#loginButton').click(loginUser);

this.loginButton = function (event) {
    var username = $('#loginUsername').val() || undefined;
    var password = $('#loginPassword').val() || undefined;

    Parse.User.logIn(username, password, {
        success: function(user) {
            //once they login successfully,put code for what to do next here    
            alert("Yeay, you logged in successfully!");
        },
        error: function(user, error) {
            // The login failed. Check error to see why.
            alert("Failed to login: " + error.message);
        }
    });
}
</script>
</login>